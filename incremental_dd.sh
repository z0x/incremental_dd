#!/bin/bash
reset && clear && tput sc
CWD=$( pwd )
CONFIG_PATH="$CWD/.config"
CONFIG_FILE="$CONFIG_PATH/config"
SESSION_PATH="$CONFIG_PATH/sessions"

if [ ! -e "$CONFIG_PATH" ] ; then
    mkdir "$CONFIG_PATH"
fi
if [ ! -e "$SESSION_PATH" ] ; then
    mkdir "$SESSION_PATH"
fi
if [ ! -e "$CONFIG_FILE" ] ; then
    touch "$CONFIG_FILE"
fi

mapfile -t DRIVE_INFO_ARRAY < <( lsblk -nl | grep disk | awk '{print $1}' )
DRIVE_INFO_LIST=$( lsblk -nl | grep disk | awk '{print $1}' )
#### the above is like this because i can't get
# consistent array behavior. so i have one 'list' iterable
# and one array that i can access by element number.
# This is probably very confusing. sorry.
# PR's are welcome to fix it.
#
EXISTING_SESSIONS_LIST=$(<"$CONFIG_FILE")

function reset_and_clear(){
    tput rc && tput ed
    TOPLINE="incremental_dd 0.0.1.x: $1\n©2018 z0x\n"
    echo -e "$TOPLINE"
}

function print_drives(){
    DRIVE_ARRAY_COUNTER=0
    for i in ${1} ; do :
        DEVICE_PATH="/dev/$i"
        DRIVE_INFO=$( udevadm info --query=all --name="$DEVICE_PATH" )
        ID_MODEL=$( grep "\<ID_MODEL\>" <<< "$DRIVE_INFO"  | awk 'BEGIN { FS="="} {print $2}' )
        ID_SERIAL=$( grep "\<ID_SERIAL\>" <<< "$DRIVE_INFO" | awk 'BEGIN { FS="="} {print $2}' )
        ID_SERIAL_SHORT=$( grep "\<ID_SERIAL_SHORT\>" <<< "$DRIVE_INFO" | awk 'BEGIN { FS="="} {print $2}' )
        DRIVE_SIZE=$( lsblk -nl "$DEVICE_PATH" | awk 'NR==1 { print $4 }' )
        echo -e "#$DRIVE_ARRAY_COUNTER)"
        echo -e "\tMount Point:\t/dev/$i"
        echo -e "\tModel:\t\t$ID_MODEL"
        echo -e "\tSerial:\t\t$ID_SERIAL"
        echo -e "\tShort Serial:\t$ID_SERIAL_SHORT"
        echo -e ""
        let "DRIVE_ARRAY_COUNTER+=1"
    done
    return "$DRIVE_ARRAY_COUNTER"
}

function incremental_dd(){ #$1=$SESSION_NAME $2=$SESSION_PATH $3=$INPUT_DRIVE_PATH $4 OUTPUT_DRIVE_PATH
    SHUTDOWN=0
    SESSION_NAME=$1
    SESSION_PATH=$2
    INPUT_DRIVE_PATH=$3
    OUTPUT_DRIVE_PATH=$4
    FINISHED=0
    SKIP=0
    source "$SESSION_PATH/$SESSION_NAME"

    while [ "$FINISHED" -ne 1 ] ; do
        reset_and_clear "Working..."
        let "BYTES_COPIED = BLOCK_SIZE * BLOCKS_PER_ROUND * ROUNDS_COMPLETED"

        if [ ! -e "$INPUT_DRIVE_PATH" ] || [ ! -e "$OUTPUT_DRIVE_PATH" ] ; then
            reset_and_clear "ERROR - DRIVE VANISHED - SHUTTING DOWN"
            echo -e "Err. Press return to shut down"
            read
            exit 1
        fi

        if [ "$BYTES_COPIED" -lt "$INPUT_DRIVE_SIZE_BYTES" ] ; then
            trap "SHUTDOWN=1" 2
            let "SKIP=$ROUNDS_COMPLETED*$BLOCKS_PER_ROUND"
            SEEK=$SKIP

            sudo dd "if=$INPUT_DRIVE_PATH" "of=$OUTPUT_DRIVE_PATH" "seek=$SEEK" "skip=$SKIP" "count=$BLOCKS_PER_ROUND"

            let "ROUNDS_COMPLETED++"

            : > "$SESSION_PATH/$SESSION_NAME"
            echo -e "ROUNDS_COMPLETED=$ROUNDS_COMPLETED\nBLOCK_SIZE=$BLOCK_SIZE\nBLOCKS_PER_ROUND=$BLOCKS_PER_ROUND\nINPUT_DRIVE_SIZE_BYTES=$INPUT_DRIVE_SIZE_BYTES" \
                >> "$SESSION_PATH/$SESSION_NAME"

            if [ "$BYTES_COPIED" -eq 0 ] ; then
                PCT_COMPLETE=0
            else
                PCT_COMPLETE=$(( 100 * BYTES_COPIED / INPUT_DRIVE_SIZE_BYTES + (1000 * BYTES_COPIED / INPUT_DRIVE_SIZE_BYTES % 10 >= 5 ? 1 : 0) ))
            fi

            echo -e "incremental_dd session: #0)"
            echo -e "Rounds_Completed: $ROUNDS_COMPLETED | Block size: $BLOCK_SIZE | Blocks per round: $BLOCKS_PER_ROUND"
            echo -e "Input drive size: $INPUT_DRIVE_SIZE_BYTES | $BYTES_COPIED bytes copied |  $PCT_COMPLETE% complete"
            echo -e "\nInput Drive:"
            echo -e "\t$INPUT_DRIVE_PATH"
            echo -e "\nOutput Drive: "
            echo -e "\t$OUTPUT_DRIVE_PATH"
            echo -e "\n ctrl + c saves and exits"

            if [ "$SHUTDOWN" -ne 0 ] ; then
                reset_and_clear "Goodbye"
                exit 0
            fi
    else
        echo "File copied"
        FINISHED=1
    fi
done
}


function get_session_info(){ #$1=EXISTING_SESSIONS_LIST  $2=$SESSION_SELECTION $3=$DRIVE_INFO_LIST $4=$SESSIONS_PATH
    EXISTING_SESSIONS_LIST=$1
    SESSION_SELECTION=$2
    DRIVE_INFO_LIST=$3
    SESSIONS_PATH=$4
    SELECTED_SESSION_NAME=""
    SESSION_COUNT=0
    INPUT_ID_MODEL=""
    INPUT_DRIVE_PATH=""
    INPUT_DRIVE_SIZE=""
    OUTPUT_ID_MODEL=""
    OUTPUT_DRIVE_SIZE=""
    OUTPUT_DRIVE_PATH=""

    for i in ${EXISTING_SESSIONS_LIST[@]} ; do
        if [ "$SESSION_COUNT" -eq "$SESSION_SELECTION" ]  ; then
            SELECTED_SESSION_NAME="$i"
            break
        fi
        let "SESSION_COUNT++"
    done

    INPUT_ID_SERIAL_FROM_CONFIG=$( printf "$i" | awk 'BEGIN { FS="+" } { print $1 }' )
    OUTPUT_ID_SERIAL_FROM_CONFIG=$( printf "$i" | awk 'BEGIN { FS="+" } { print $2 }' )
    for j in ${DRIVE_INFO_LIST[@]} ; do :
                    DEVICE_PATH="/dev/$j"
                    DRIVE_INFO=$( udevadm info --query=all --name="$DEVICE_PATH" )
                    ID_SERIAL=$( grep "\<ID_SERIAL\>" <<< "$DRIVE_INFO" | awk 'BEGIN { FS="="} {print $2}' )

                    if [ "$ID_SERIAL" == "$INPUT_ID_SERIAL_FROM_CONFIG" ] ; then
                        INPUT_ID_MODEL=$( grep "\<ID_MODEL\>" <<< "$DRIVE_INFO"  | awk 'BEGIN { FS="="} {print $2}' )
                        INPUT_DRIVE_SIZE=$( lsblk -nl "$DEVICE_PATH" | awk 'NR==1 { print $4 }' )
                        INPUT_DRIVE_PATH="$DEVICE_PATH"
                        INPUT_ID_SERIAL="$ID_SERIAL"
                    fi

                    if [ "$ID_SERIAL" == "$OUTPUT_ID_SERIAL_FROM_CONFIG" ] ; then
                        OUTPUT_ID_MODEL=$( grep "\<ID_MODEL\>" <<< "$DRIVE_INFO"  | awk 'BEGIN { FS="="} {print $2}' )
                        OUTPUT_DRIVE_SIZE=$( lsblk -nl "$DEVICE_PATH" | awk 'NR==1 { print $4 }' )
                        OUTPUT_DRIVE_PATH="$DEVICE_PATH"
                        OUTPUT_ID_SERIAL="$ID_SERIAL"
                    fi
                done

        reset_and_clear "Confirm Selected Session"
        source "$SESSIONS_PATH/$SELECTED_SESSION_NAME"
        let "BYTES_COPIED=ROUNDS_COMPLETED*BLOCK_SIZE*BLOCKS_PER_ROUND"
        if [ "$BYTES_COPIED" -eq 0 ] ; then
            PCT_COMPLETE=0
        else
                    PCT_COMPLETE=$(( 100 * BYTES_COPIED / INPUT_DRIVE_SIZE_BYTES + (1000 * BYTES_COPIED / INPUT_DRIVE_SIZE_BYTES % 10 >= 5 ? 1 : 0) ))
        fi
        echo -e "incremental_dd session: #$SESSION_SELECTION)"
        echo -e "Input Drive:"
        echo -e "\t$INPUT_ID_MODEL"
        echo -e "\t$INPUT_ID_SERIAL"
        echo -e "\t$INPUT_DRIVE_PATH $INPUT_DRIVE_SIZE"
        echo -e "Output Drive: "
        echo -e "\t$OUTPUT_ID_MODEL"
        echo -e "\t$OUTPUT_ID_SERIAL"
        echo -e "\t$OUTPUT_DRIVE_PATH $OUTPUT_DRIVE_SIZE"
        echo -e "Rounds_Completed: $ROUNDS_COMPLETED | Block size: $BLOCK_SIZE | Blocks per round: $BLOCKS_PER_ROUND"
        echo -e "Input drive size: $INPUT_DRIVE_SIZE_BYTES | $BYTES_COPIED bytes copied | $PCT_COMPLETE% complete"
        echo -e ""
        echo -e "Press return to begin copying. ctrl +c to quit"
        read

        if [ ! -e "$INPUT_DRIVE_PATH" ] || [ ! -e "$OUTPUT_DRIVE_PATH" ] ; then
            reset_and_clear "ERROR - MISSING DRIVES"
            echo -e "Cannot Start Session. Drive(s) not found. press return to choose another session"
        else
            incremental_dd "$SELECTED_SESSION_NAME" "$SESSIONS_PATH" "$INPUT_DRIVE_PATH" "$OUTPUT_DRIVE_PATH"
        fi
}

function select_session(){ #$1=$CONFIG_FILE $2=$DRIVE_INFO_LIST $3=EXISTING_SESSIONS_LIST $4=$SESSIONS_PATH
    if [ -s "$1" ] ; then
        reset_and_clear "Choose Session"
        DRIVE_INFO_LIST=$2
        EXISTING_SESSIONS_LIST=$3
        SESSIONS_PATH=$4
        SESSION_COUNT=0
        RESUMABLE_SESSION_COUNT=0
        SESSION_SELECTION=""
        echo -e "Existing Sessions Found"
        echo -e ""
            for i in ${EXISTING_SESSIONS_LIST[@]} ; do :
                INPUT_DRIVE_ATTACHED=0
                OUTPUT_DRIVE_ATTACHED=0
                INPUT_ID_SERIAL_FROM_CONFIG=$( printf "$i" | awk 'BEGIN { FS="+" } { print $1 }' )
                INPUT_ID_SERIAL=""
                INPUT_DRIVE_SIZE=""
                INPUT_DRIVE_PATH=""
                INPUT_ID_MODEL=""
                OUTPUT_ID_SERIAL_FROM_CONFIG=$( printf "$i" | awk 'BEGIN { FS="+" } { print $2 }' )
                OUTPUT_ID_SERIAL=""
                OUTPUT_DRIVE_SIZE=""
                OUTPUT_DRIVE_PATH=""
                OUTPUT_DRIVE_MODEL=""
                for j in ${DRIVE_INFO_LIST[@]} ; do :
                    DEVICE_PATH="/dev/$j"
                    DRIVE_INFO=$( udevadm info --query=all --name="$DEVICE_PATH" )
                    ID_SERIAL=$( grep "\<ID_SERIAL\>" <<< "$DRIVE_INFO" | awk 'BEGIN { FS="="} {print $2}' )

                    if [ "$ID_SERIAL" == "$INPUT_ID_SERIAL_FROM_CONFIG" ] ; then
                        INPUT_ID_MODEL=$( grep "\<ID_MODEL\>" <<< "$DRIVE_INFO"  | awk 'BEGIN { FS="="} {print $2}' )
                        INPUT_DRIVE_SIZE=$( lsblk -nl "$DEVICE_PATH" | awk 'NR==1 { print $4 }' )
                        INPUT_DRIVE_PATH="$DEVICE_PATH"
                        INPUT_ID_SERIAL="$ID_SERIAL"
                        INPUT_DRIVE_ATTACHED=1
                    fi

                    if [ "$ID_SERIAL" == "$OUTPUT_ID_SERIAL_FROM_CONFIG" ] ; then
                        OUTPUT_ID_MODEL=$( grep "\<ID_MODEL\>" <<< "$DRIVE_INFO"  | awk 'BEGIN { FS="="} {print $2}' )
                        OUTPUT_DRIVE_SIZE=$( lsblk -nl "$DEVICE_PATH" | awk 'NR==1 { print $4 }' )
                        OUTPUT_DRIVE_PATH="$DEVICE_PATH"
                        OUTPUT_ID_SERIAL="$ID_SERIAL"
                        OUTPUT_DRIVE_ATTACHED=1
                    fi

                    let "DRIVE_ARRAY_COUNT+=1"
                done

                if [ "$INPUT_DRIVE_ATTACHED"  -eq 1 ] && [ "$OUTPUT_DRIVE_ATTACHED" -eq 1 ] ; then
                    echo -e "Session #$SESSION_COUNT)"
                    source "$SESSIONS_PATH/$INPUT_ID_SERIAL+$OUTPUT_ID_SERIAL"
                    let "BYTES_COPIED=ROUNDS_COMPLETED*BLOCK_SIZE*BLOCKS_PER_ROUND"

                if [ "$BYTES_COPIED" -eq 0 ] ; then
                    PCT_COMPLETE=0
                else
                    PCT_COMPLETE=$(( 100 * BYTES_COPIED / INPUT_DRIVE_SIZE_BYTES + (1000 * BYTES_COPIED / INPUT_DRIVE_SIZE_BYTES % 10 >= 5 ? 1 : 0) ))
                fi

                echo -e "Input Drive:"
                echo -e "\t$INPUT_ID_MODEL"
                echo -e "\t$INPUT_ID_SERIAL"
                echo -e "\t$INPUT_DRIVE_PATH $INPUT_DRIVE_SIZE"
                echo -e "Output Drive: "
                echo -e "\t$OUTPUT_ID_MODEL"
                echo -e "\t$OUTPUT_ID_SERIAL"
                echo -e "\t$OUTPUT_DRIVE_PATH $OUTPUT_DRIVE_SIZE"
                echo -e "Rounds_Completed: $ROUNDS_COMPLETED | Block size: $BLOCK_SIZE | Blocks per round: $BLOCKS_PER_ROUND"
                echo -e "Input drive size: $INPUT_DRIVE_SIZE_BYTES | $BYTES_COPIED bytes copied | $PCT_COMPLETE% complete"
                echo -e ""
                let "RESUMABLE_SESSION_COUNT++"
                fi

                let "SESSION_COUNT++"
            done

            if [ "$RESUMABLE_SESSION_COUNT" -eq 0 ] ; then
                echo "But none could be resumed. Hit return to start a new session or reconnect drives and restart this script"
                read
                break
            else
                echo "Select Session to Resume or hit return  for a new session"
                let "SESSION_COUNT--"
                read SESSION_SELECTION
                SESSION_SELECTION_SANITIZED=$( tr -dc [:digit:] <<< "$SESSION_SELECTION" )
                if [ -z "$SESSION_SELECTION_SANITIZED" ] || [ ! "$SESSION_SELECTION_SANITIZED" -le "$SESSION_COUNT" ] ; then
                    pintf ""
                else
                    get_session_info "$EXISTING_SESSIONS_LIST" "$SESSION_SELECTION" "$DRIVE_INFO_LIST" "$SESSIONS_PATH"
                fi
            fi
    else
        echo -e "No Sessions Found."
    fi
}

new_session(){ #$1=DRIVE_INFO_LIST $2=DRIVE_INFO_ARRAY
    reset_and_clear "New Session - Choose Input"
    DRIVE_INFO_LIST=$1
    DRIVE_INFO_ARRAY=$2
    SESSION_PATH=$3
    ROUNDS_COMPLETED=0 #sigh
    INPUT_DRIVE_SELECT=""
    DRIVE_INFO_ARRAY_COUNT=${#DRIVE_INFO_ARRAY[@]}
    let "DRIVE_INFO_ARRAY_COUNT--"

    echo -e "Select Input and Output drives to start a new session."
    echo -e ""
    echo -e "Select Input Drive"
    echo -e ""
    print_drives "$DRIVE_INFO_LIST"
    read INPUT_DRIVE_SELECT
    INPUT_DRIVE_SELECT_SANITIZED=$( tr -dc [:digit:] <<< "$INPUT_DRIVE_SELECT" )
    if [ ! -z "$INPUT_DRIVE_SELECT_SANITIZED" ] && [ ! $INPUT_DRIVE_SELECT_SANITIZED -gt $DRIVE_INFO_ARRAY_COUNT ] ; then
        printf ""
    else
        echo -e "bad input. press return to try again."
        read
        new_session "$DRIVE_INFO_LIST" "$DRIVE_INFO_ARRAY"
    fi

    INPUT_DRIVE_PATH="/dev/${DRIVE_INFO_ARRAY["$INPUT_DRIVE_SELECT"]}"
    INPUT_DRIVE_INFO=$( udevadm info --query=all --name="$INPUT_DRIVE_PATH" )
    INPUT_DRIVE_ID_MODEL=$( grep "\<ID_MODEL\>" <<< "$INPUT_DRIVE_INFO"  | awk 'BEGIN { FS="="} {print $2}' )
    INPUT_DRIVE_ID_SERIAL=$( grep "\<ID_SERIAL\>" <<< "$INPUT_DRIVE_INFO" | awk 'BEGIN { FS="="} {print $2}' )
    INPUT_DRIVE_ID_SERIAL_SHORT=$( grep "\<ID_SERIAL_SHORT\>" <<< "$INPUT_DRIVE_INFO" | awk 'BEGIN { FS="="} {print $2}' )

    reset_and_clear "New Session - Choose Output"
    echo -e "Input drive: $INPUT_DRIVE_ID_MODEL $INPUT_DRIVE_PATH $INPUT_DRIVE_ID_SERIAL_SHORT"
    echo -e ""
    echo -e "Select Output Drive\n"
    print_drives "$DRIVE_INFO_LIST"
    read OUTPUT_DRIVE_SELECT
    OUTPUT_DRIVE_SELECT_SANITIZED=$( tr -dc [:digit:] <<< "$OUTPUT_DRIVE_SELECT" )
    if [ ! -z "$OUTPUT_DRIVE_SELECT_SANITIZED" ] && [ "$OUTPUT_DRIVE_SELECT_SANITIZED" -le "$DRIVE_INFO_ARRAY_COUNT" ] ; then
        pintf ""
    else
        echo -e "bad input. press return to try again"
        read
        new_session "$DRIVE_INFO_LIST" "$DRIVE_INFO_ARRAY"
    fi
    OUTPUT_DRIVE_PATH="/dev/${DRIVE_INFO_ARRAY["$OUTPUT_DRIVE_SELECT"]}"
    OUTPUT_DRIVE_INFO=$( udevadm info --query=all --name="$OUTPUT_DRIVE_PATH" )
    OUTPUT_DRIVE_ID_MODEL=$( grep "\<ID_MODEL\>" <<< "$OUTPUT_DRIVE_INFO"  | awk 'BEGIN { FS="="} {print $2}' )
    OUTPUT_DRIVE_ID_SERIAL=$( grep "\<ID_SERIAL\>" <<< "$OUTPUT_DRIVE_INFO" | awk 'BEGIN { FS="="} {print $2}' )
    OUTPUT_DRIVE_ID_SERIAL_SHORT=$( grep "\<ID_SERIAL_SHORT\>" <<< "$OUTPUT_DRIVE_INFO" | awk 'BEGIN { FS="="} {print $2}' )

    SESSION_NAME="$INPUT_DRIVE_ID_SERIAL+$OUTPUT_DRIVE_ID_SERIAL"

    reset_and_clear "New Session - Customize Block Size"
    echo -e "Input drive: $INPUT_DRIVE_ID_MODEL $INPUT_DRIVE_PATH $INPUT_DRIVE_ID_SERIAL_SHORT"
    echo -e "Output drive: $OUTPUT_DRIVE_ID_MODEL $OUTPUT_DRIVE_PATH $OUTPUT_DRIVE_ID_SERIAL_SHORT"
    echo -e ""
    echo -e "Enter custom block size, or hit return for default (512)"
    read BLOCK_SIZE
    BLOCK_SIZE_SANITIZED=$( tr -dc [:digit:] <<< "$BLOCK_SIZE" )
    if [ "$BLOCK_SIZE_SANITIZED" == "" ] ; then
        BLOCK_SIZE=512
    fi

    reset_and_clear "New Session - Customize blocks per iteration"
    echo -e "Input drive: $INPUT_DRIVE_ID_MODEL $INPUT_DRIVE_PATH $INPUT_DRIVE_ID_SERIAL_SHORT"
    echo -e "Output drive: $OUTPUT_DRIVE_ID_MODEL $OUTPUT_DRIVE_PATH $OUTPUT_DRIVE_ID_SERIAL_SHORT"
    echo -e "Block size: $BLOCK_SIZE"
    echo -e ""
    echo -e "Enter the desired number of blocks copied per iteration, or hit return for default (1)"
    read BLOCKS_PER_ROUND
    BLOCKS_PER_ROUND_SANITIZED=$( tr -dc [:digit:] <<< "$BLOCKS_PER_ROUND" )
    if [ "$BLOCKS_PER_ROUND_SANITIZED" == "" ] ; then
        BLOCKS_PER_ROUND=1
    fi

    reset_and_clear "New Session - Enter password"
    echo -e "Input drive: $INPUT_DRIVE_ID_MODEL $INPUT_DRIVE_PATH $INPUT_DRIVE_ID_SERIAL_SHORT"
    echo -e "Output drive: $OUTPUT_DRIVE_ID_MODEL $OUTPUT_DRIVE_PATH $OUTPUT_DRIVE_ID_SERIAL_SHORT"
    echo -e "Block size: $BLOCK_SIZE | Blocks per round $BLOCKS_PER_ROUND"
    echo -e ""
    echo -e "Now I need to get some info about the input drive only available to superusers, the size in bytes."
    echo -e ""
    echo -e "Please enter your password if prompted below. It will not be stored or transmitted."
    echo -e ""
    sleep 3 #its unnerving for a screen to flash by when you've got an active sudoers session
    INPUT_DRIVE_SIZE_BYTES=$(sudo parted "$INPUT_DRIVE_PATH" unit B print | awk 'FNR==2 { print $3 }' | tr -dc [:digit:])
    OUTPUT_DRIVE_SIZE_BYTES=$(sudo parted "$OUTPUT_DRIVE_PATH" unit B print | awk 'FNR==2 { print $3 }' | tr -dc [:digit:])
    reset_and_clear "New Session - Superfluous info"
    echo -e "Your $INPUT_DRIVE_ID_MODEL is $INPUT_DRIVE_SIZE_BYTES bytes long."
    echo -e "And your $OUTPUT_DRIVE_ID_MODEL is $OUTPUT_DRIVE_SIZE_BYTES long."
    echo -e "In case you were wondering."
    echo -e ""

    if [ "$OUTPUT_DRIVE_SIZE_BYTES" -lt "$INPUT_DRIVE_SIZE_BYTES" ] ; then
        reset_and_clear "Error - Drive size Mismatch"
        echo -e "Output drive too small. Press return to start over"
        read
        new_session "$DRIVE_INFO_LIST" "$DRIVE_INFO_ARRAY"
    fi
    if [ "$INPUT_DRIVE_PATH" == "$OUTPUT_DRIVE_PATH" ] ; then
        reset_and_clear "ERROR - SAME DRIVE"
        echo -e "Its kind of pointless to copy a drive byte for byte onto itself."
        echo -e "Unless you're *actively* trying to shred your drive"
        echo -e "In that case, comment out this if block."
        echo -e ""
        echo -e "Otherwise, Press enter to start a new session."
        read
        new_session "$DRIVE_INFO_LIST" "$DRIVE_INFO_ARRAY"
    fi
    if [ -e "$SESSION_PATH/$SESSION_NAME" ] ; then
        echo -e "WARNING: SESSION ALREADY EXISTS. CONTINUING DESTROYS OLD SESSION"
        echo -e "Press return to save this session. ctrl + c quits"
        read
        rm "$SESSION_PATH/$SESSION_NAME"
        touch "$SESSION_PATH/$SESSION_NAME"
    else
        echo -e "Press return to save this session. ctrl + c quits"
        read
        touch "$SESSION_PATH/$SESSION_NAME"
        echo "$SESSION_NAME" >> "$CONFIG_FILE"
    fi

    echo -e "ROUNDS_COMPLETED=$ROUNDS_COMPLETED\nBLOCK_SIZE=$BLOCK_SIZE\nBLOCKS_PER_ROUND=$BLOCKS_PER_ROUND\nINPUT_DRIVE_SIZE_BYTES=$INPUT_DRIVE_SIZE_BYTES" \
        >> "$SESSION_PATH/$SESSION_NAME"

    SESSION_TOTAL=$( ls "$SESSION_PATH" | wc -l )
    let "SESSION_SELECTION=SESSION_TOTAL-1"
    EXISTING_SESSIONS_LIST=$(<"$CONFIG_FILE")

    get_session_info "$EXISTING_SESSIONS_LIST" "$SESSION_SELECTION" "$DRIVE_INFO_LIST" "$SESSION_PATH"
}

select_session "$CONFIG_FILE" "$DRIVE_INFO_LIST" "$EXISTING_SESSIONS_LIST" "$SESSION_PATH"
new_session "$DRIVE_INFO_LIST" "$DRIVE_INFO_ARRAY" "$SESSION_PATH"