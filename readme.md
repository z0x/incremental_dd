# incremental_dd

#### About
`incremental_dd.sh` will make a drive to drive copy
within a persistent, configurable `dd` session.
#### Usage

Simply `./incremental_dd.sh` to start the script. Currently takes no arguments.

I _highly_ recommend setting the block size and blocks per round to something reasonable to reduce copy time and disk wear.

For me, (over usb2, with this crappy drive housing) i would set `block size = 3000` and `blocks per round = 1000`. So, `3000 bytes * 1000 rounds = 3MB` or, just over one round per second on my system (don't hate)

`ctrl + c` exits.

#### Requirements
* Bash 4+ (zsh should work, too)

#### Releases
[0.0.1.0](https://gitlab.com/z0x/incremental_dd/tree/0.0.0.0)
* UX straight out of 1972
* able to handle multiple sessions
* verifies input
* generally prevents user from being a shithead.

[0.0.0.0](https://gitlab.com/z0x/incremental_dd/tree/0.0.0.0)
* first working prototype.
* works on files not drives
* one session at a time

#### Caveats
* It should go without saying, but it is very important the input and output drives do not change between sessions

* Since we're copying drives we're assuming we need sudo privileges.

* `ctrl + c (SIGINT)` is the only way to exit gracefully. Any other method of killing the script is undefined and may screw up your session file.
